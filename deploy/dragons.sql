-- Deploy doubledragon:dragons to pg
-- requires: ddschema

BEGIN;

SET client_min_messages = 'warning';

CREATE TABLE doubledragon.dragons(
  username  TEXT        PRIMARY KEY,
  password  TEXT        NOT NULL,
  timestamp TIMESTAMPTZ NOT NULL DEFAULT NOW()
);

COMMIT;
