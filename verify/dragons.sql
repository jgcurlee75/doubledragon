-- Verify doubledragon:dragons on pg

BEGIN;

SELECT username, password, timestamp 
  FROM doubledragon.dragons
 WHERE FALSE;

ROLLBACK;
