-- Verify doubledragon:ddschema on pg

BEGIN;

SELECT pg_catalog.has_schema_privilege('doubledragon', 'usage');

ROLLBACK;
